Vagrant box with WordPress and REST-API based React theme "Picard"
==================================================================
We use this as a boilerplate to get new development projects up and running in seconds to demo a REACT-theme created by Jack Lenox, first shown at WordCamp London 2015.

This is a Vagrant setup with:
* [ScotchBox](http://box.scotch.io)
* [Roots Bedrock](https://roots.io/bedrock/)
* Some simple provision script setup in the Vagrantfile
* Picard theme installed in theme

### Step 1
Download, fork or clone this repo to your local machine, eg 
```
git clone https://github.com/ekandreas/scotch-bedrock thenewproject 
```
### Step 2
Change settings in the two lines of Vagrantfile. eg, 
```
    hostname = "wprest"
    ip = "192.168.33.111"
```
### Step 3
Run Vagrant! 
```
vagrant up
```
### Step 4
Ignore the red lines of install messages and missing sendmail!

### Step 5
Browse to your hostname.dev, eg 
```
http://wprest.dev
```
### Step 6
Log in to WordPress with username *admin* and password *admin* at /wp/wp-admin, eg 
```
http://wprest.dev/wp/wp-admin
admin / admin
```
### Step 7
Change permalinks to: /%year%/%monthnum%/%day%/%postname%/
Activate Picard under Appearance->Themes